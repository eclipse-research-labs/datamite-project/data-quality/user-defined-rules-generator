.. raw:: html

   <!--
     ~ Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)
     ~ 
     ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
     ~ this software and associated documentation files (the "Software"), to deal in
     ~ the Software without restriction, including without limitation the rights to
     ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
     ~ the Software, and to permit persons to whom the Software is furnished to do so,
     ~ subject to the following conditions:
     ~ 
     ~ The above copyright notice and this permission notice shall be included in all
     ~ copies or substantial portions of the Software.
     ~ 
     ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
     ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
     ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
     ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     ~ 
     ~ SPDX-License-Identifier: MIT
   -->

DESCRIPTION
===========

This project will host the developments for the User Defined Rules
Generator, one of the components of the Data Quality Module, and which
is included in the components marked in the architecture figure below.

.. figure:: doc/figures/arch_extended_KPIRules.png
   :alt: architecture_KPIRules

   architecture_KPIRules

REST API of the User Defined Rules Generator
--------------------------------------------

The User Defined Rules Generator (UDRG) can be installed via docker
compose. The composition includes: - A Mongo database. - The UDRG with
its HTTP API.

The HTTP API offered by the UDRG is specified
`here <./doc/user-defined-rules-generator-openapi.yaml>`__.

Dataset and KPIs sample files to test the HTTP API can be found
`here <./app/examples/>`__. ### Installation To install the project, you
need to have Docker and Docker-compose installed on your machine. If you
don’t have it, you can install it by following the instructions on the
official Docker website. #### Get the code Clone this repository:

.. code:: bash

   git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator.git

Configure properties files
~~~~~~~~~~~~~~~~~~~~~~~~~~

Check the ``user_defined_rules.env`` file and change the property values
as required.

Start containers
~~~~~~~~~~~~~~~~

Create and start the containers specified in the ``docker-compose.yml``
file:

.. code:: bash

   docker-compose up -d

Rules interpreter as Python library
-----------------------------------

The user defined rules interpreter is also provided as a Python library.

Installation
~~~~~~~~~~~~

Generate the wheel file

.. code:: bash

   cd lib
   python setup.py bdist_wheel --universal

Install library:

.. code:: bash

   cd lib/dist
   pip3 install ruleinterpreter-0.1-py2.py3-none-any.whl

Usage
~~~~~

Check if dataset has active rules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The library provides the ``check_dataset_active_custom_metrics`` method
to check if the dataset has active rules associated. It can be invoked
as shown in the following example:

::

   from ruleinterpreter.rule_interpreter import RuleInterpreter

   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   check_active_rules = ri.check_dataset_active_custom_metrics(dataset_id='ds1')
   print(f'check_active_rules={check_active_rules}')

Get required KPI-s
^^^^^^^^^^^^^^^^^^

The library provides the ``get_custom_metrics_req_kpis`` method to get
the names of the KPI-s required by the custom metrics associated to a
specific dataset. It can be invoked as shown in the following example:

::

   import json
   from ruleinterpreter.rule_interpreter import RuleInterpreter

   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   kpis_names_list = ri.get_custom_metrics_req_kpis(dataset_id='ds1')
   print(json.dumps(kpis_names_list, indent=2))

Output:

::

   [
     {
       "col": "Temperature",
       "kpi": "numeric_mean"
     },
     {
       "col": "Humidity",
       "kpi": "numeric_mean"
     },
     {
       "col": "TempMax",
       "kpi": "numeric_mean"
     },
     {
       "col": "TempMin",
       "kpi": "numeric_mean"
     }
   ]

It is also possible to invoke this method to get the required KPI-s for
some specific custom metrics. In this case, the list of custom metrics
identifiers is provided as input parameter:

::

   import json
   from ruleinterpreter.rule_interpreter import RuleInterpreter

   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   custom_metrics_list = ['https://doi.org/Dataset/meteo/CustomMetric/Ex1', 'https://doi.org/Dataset/meteo/CustomMetric/Ex2']
   kpis_names_list=ri.get_custom_metrics_req_kpis(dataset_id='ds1', custom_metrics_list=custom_metrics_list)
   print(json.dumps(kpis_names_list, indent=2))

Output:

::

   [
     {
       "col": "Humidity",
       "kpi": "numeric_mean"
     }
   ]

Get required columns
^^^^^^^^^^^^^^^^^^^^

The library provides the ``get_custom_metrics_req_cols`` method to get
the names of the dataset columns required by the custom metrics
associated to a specific dataset. It can be invoked as shown in the
following example:

::

   import json
   from ruleinterpreter.rule_interpreter import RuleInterpreter

   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   cols_names_list = ri.get_custom_metrics_req_cols(dataset_id='ds1')
   print(json.dumps(cols_names_list, indent=2))

Output:

::

   [
     "City",
     "Temperature",
     "Email",
     "Humidity",
     "DateTime",
     "TempMax",
     "TempMin"
   ]

It is also possible to invoke this method to get the required dataset
columns for some specific custom metrics. In this case, the list of
custom metrics identifiers is provided as input parameter:

::

   import json
   from ruleinterpreter.rule_interpreter import RuleInterpreter

   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   custom_metrics_list = ['https://doi.org/Dataset/meteo/CustomMetric/Ex1', 'https://doi.org/Dataset/meteo/CustomMetric/Ex2']
   cols_names_list=ri.get_custom_metrics_req_cols(dataset_id='ds1', custom_metrics_list=custom_metrics_list)
   print(json.dumps(cols_names_list, indent=2))

Output:

::

   [
     "Humidity"
   ]

Rules evaluation
^^^^^^^^^^^^^^^^

The library provides the ``compute_custom_metrics`` method to evaluate
the active rules associated to a dataset. For doing so, first a pandas
DataFrame with the data to be analyzed and another pandas dataframe with
the already calculated KPIs are created, and then they are provided as
input parameters, together with the dataset identifier, to this method.

For example:

::

   import json
   import pandas as pd
   from ruleinterpreter.rule_interpreter import RuleInterpreter

   data = [['Bilbao', -10, 120, 35, 0, '2023-05-30T09:00:00', 'email1@gmail.com'], ['Madrid', 45, 70, 40, -5, '2023-05-30T09:00:00', 'email2@gmail.com'],
           ['Barcelona', 14, 60, 45, 1, '2023-05-30T09:00:00', 'email3@gmail.com'], ['London', 5, 50, 30, -10, '2023-05-30T09:00:00', 'email4@gmail.com'],
           ['Paris', 4, 130, 38, -12, '2023-05-30T09:00:00', 'email5']]
   data_df = pd.DataFrame(data, columns=['City', 'Temperature', 'Humidity', 'TempMax', 'TempMin', 'DateTime', 'Email'])
   kpis = [['Temperature', 'numeric_mean', 19.5], ['Humidity', 'numeric_mean', 60.3], ['TempMax', 'numeric_mean', 32.0],
           ['TempMin', 'numeric_mean', 5.0]]
   kpis_df = pd.DataFrame(kpis, columns=['col', 'kpi', 'value'])
   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   measurements_list = ri.compute_custom_metrics(dataset_id='ds1', data_df=data_df, kpis_df=kpis_df)
   print(json.dumps(measurements_list, indent=2))

Output:

::

   [
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/tempValueRange",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 0.0,
       "dcterms_title": "Temperature accepted range values."
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/tempMeanRange",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 100.0,
       "dcterms_title": "Temperature mean accepted range values."
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/Ex1",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 0.0,
       "dcterms_title": "Accepted range values and mean value for humidity."
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/Ex2",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 80.0,
       "dcterms_title": "Accepted range values and mean value for humidity."
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/Ex3",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 60.0,
       "dcterms_title": "Temperature, TempMax, TempMin  accepted range values."
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/Ex4",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 80.0,
       "dcterms_title": "KPI mean <= columnValue"
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/DateTimeValueRange",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 0.0,
       "dcterms_title": "DateTime accepted range values."
     }
   ]

It is also possible to invoke this method to evaluate some specific
custom metrics. In this case, the list of custom metrics identifiers is
provided as input parameter: For example:

::

   import json
   import pandas as pd
   from ruleinterpreter.rule_interpreter import RuleInterpreter

   data = [['Bilbao', -10, 120, 35, 0, '2023-05-30T09:00:00', 'email1@gmail.com'], ['Madrid', 45, 70, 40, -5, '2023-05-30T09:00:00', 'email2@gmail.com'],
           ['Barcelona', 14, 60, 45, 1, '2023-05-30T09:00:00', 'email3@gmail.com'], ['London', 5, 50, 30, -10, '2023-05-30T09:00:00', 'email4@gmail.com'],
           ['Paris', 4, 130, 38, -12, '2023-05-30T09:00:00', 'email5']]
   data_df = pd.DataFrame(data, columns=['City', 'Temperature', 'Humidity', 'TempMax', 'TempMin', 'DateTime', 'Email'])
   kpis = [['Temperature', 'numeric_mean', 19.5], ['Humidity', 'numeric_mean', 60.3], ['TempMax', 'numeric_mean', 32.0],
           ['TempMin', 'numeric_mean', 5.0]]
   kpis_df = pd.DataFrame(kpis, columns=['col', 'kpi', 'value'])
   ri = RuleInterpreter(
        rules_db_hostname=rules_db_hostname,
        rules_db_port=rules_db_port,
        rules_db_username=rules_db_username,
        rules_db_password=rules_db_password,
        rules_db_dbname=rules_db_dbname
   )
   custom_metrics_list = ['https://doi.org/Dataset/meteo/CustomMetric/Ex1', 'https://doi.org/Dataset/meteo/CustomMetric/Ex2']
   measurements_list = ri.compute_custom_metrics(dataset_id='ds1', data_df=data_df, kpis_df=kpis_df, custom_metrics_list=custom_metrics_list)
   print(json.dumps(measurements_list, indent=2))

Output:

::

   [
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/Ex1",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 0.0,
       "dcterms_title": "Accepted range values and mean value for humidity."
     },
     {
       "dqv_isMeasurementOf": "https://doi.org/Dataset/meteo/CustomMetric/Ex2",
       "dqv_computedOn": "ds1",
       "rdf_datatype": "xsd:decimal",
       "ddqv_hasParameters": [],
       "dqv_value": 80.0,
       "dcterms_title": "Accepted range values and mean value for humidity."
     }
   ]

Roadmap
-------

- ☒ API Definition (Swagger).
- ☒ Quality model definition (2nd version).
- ☐ “Create Rule” method integrated with catalog, front-end and KPI
  Library (1st version).
- ☐ “Rule interpreter” method integrated with catalog, front-end and KPI
  Library (1st version).

LICENSE
-------

MIT
