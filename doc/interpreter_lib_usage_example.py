# Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT

import json
import os
import pandas as pd

from ruleinterpreter.rule_interpreter import RuleInterpreter

data = [['Bilbao', -10, 120, 35, 0, '2023-05-30T09:00:00', 'email1@gmail.com'], ['Madrid', 45, 70, 40, -5, '2023-05-30T09:00:00', 'email2@gmail.com'],
		['Barcelona', 14, 60, 45, 1, '2023-05-30T09:00:00', 'email3@gmail.com'], ['London', 5, 50, 30, -10, '2023-05-30T09:00:00', 'email4@gmail.com'],
		['Paris', 4, 130, 38, -12, '2023-05-30T09:00:00', 'email5']]
data_df = pd.DataFrame(data, columns=['City', 'Temperature', 'Humidity', 'TempMax', 'TempMin', 'DateTime', 'Email'])
kpis = [['Temperature', 'numeric_mean', 19.5], ['Humidity', 'numeric_mean', 60.3], ['TempMax', 'numeric_mean', 32.0],
		['TempMin', 'numeric_mean', 5.0]]
kpis_df = pd.DataFrame(kpis, columns=['col', 'kpi', 'value'])

rules_db_hostname = os.getenv("DB_HOSTNAME", "datamite")
rules_db_port = os.getenv("DB_PORT", "27017")
rules_db_username = os.getenv("DB_USERNAME", "root")
rules_db_password = os.getenv("DB_PASSWORD", "datamite")
rules_db_dbname = os.getenv("DB_NAME", "datamite-quality-rules")

ri = RuleInterpreter(
	rules_db_hostname=rules_db_hostname,
	rules_db_port=rules_db_port,
	rules_db_username=rules_db_username,
	rules_db_password=rules_db_password,
	rules_db_dbname=rules_db_dbname
)

######################################################################
# Compute all rules associated to a dataset, for an artifact
######################################################################
print("######################################################################")
print("# Compute all rules associated to an artifact")
print("######################################################################")
# Invoke UDRG library to check if dataset has active rules associated
check_active_rules=ri.check_dataset_active_custom_metrics(dataset_id='ds1')
print('===========Check if active rules============')
print(f'check_active_rules={check_active_rules}')

# Invoke UDRG to get the required KPIs to evaluate the associated rules
req_kpis=ri.get_custom_metrics_req_kpis(dataset_id='ds1')
print('===========Required KPI-s============')
print(f'req_kpis={json.dumps(req_kpis, indent=2)}')

# In case the QE invokes the UDRG with only the required columns, and not the whole artifact,
# invoke UDRG to get the required column values to evaluate the associated rules
req_cols=ri.get_custom_metrics_req_cols(dataset_id='ds1')
print('===========Required Columns============')
print(f'req_cols={req_cols}')

# Compute all active rules of a dataset
measurements_list = ri.compute_custom_metrics(dataset_id='ds1', data_df=data_df, kpis_df=kpis_df)
print('===========RESULT of evaluation of all rules============')
print(json.dumps(measurements_list, indent=2))


######################################################################
# Compute specified rules for an artifact
######################################################################
print("######################################################################")
print("# Compute specified rules for an artifact")
print("######################################################################")
# Specify rules to evaluate
custom_metrics_list = ['https://doi.org/Dataset/meteo/CustomMetric/Ex1', 'https://doi.org/Dataset/meteo/CustomMetric/Ex2']
# Invoke UDRG to get the required KPIs to evaluate the associated rules
req_kpis=ri.get_custom_metrics_req_kpis(dataset_id='ds1', custom_metrics_list=custom_metrics_list)
print('===========Required KPI-s============')
print(f'req_kpis={json.dumps(req_kpis, indent=2)}')

# In case the QE invokes the UDRG with only the required columns, and not the whole artifact,
# invoke UDRG to get the required column values to evaluate the associated rules
req_cols=ri.get_custom_metrics_req_cols(dataset_id='ds1', custom_metrics_list=custom_metrics_list)
print('===========Required Columns============')
print(f'req_cols={req_cols}')

# Compute the specified rules
measurements_list = ri.compute_custom_metrics(dataset_id='ds1', data_df=data_df, kpis_df=kpis_df,
											  custom_metrics_list=custom_metrics_list)
print('===========RESULT of evaluation of specified rules============')
print(json.dumps(measurements_list, indent=2))
