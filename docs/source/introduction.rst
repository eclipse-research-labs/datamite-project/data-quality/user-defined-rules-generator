.. -------------------------------------------------------------------------------
..  MIT License
..
..  Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)
..
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
..
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
..
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
..
..  SPDX-License-Identifier: MIT
.. -------------------------------------------------------------------------------
Introduction
====

The User Defined Rules Generator (UDRG) is a component of the Data Quality module of DATAMITE framework, which togeteher with the KPI Library, and the QE compose the Data Quality Module, as depicted in the following figure.

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/raw/main/doc/figures/arch_extended_KPIRules.png


The UDRG comprises backend and frontend components, in green and orange, respectively. The interactions of the Data Quality Module with the rest of the architecture are principally with the Data Governance to access and store the quality metadata and with the Data Discovery, Ingestion and Storage tools to profile the data.

It allows the users of the framework to define business or context-aware rules that can provide specific indicators and values considered relevant within the company or use case.

Considering the necessity to interact with the user, the creation of the rules must be a process guided by the DATAMITE front-end, offering an editor for this purpose. The user would select the available indicators, taken from the KPI Library, as well as the columns that the given dataset has in its schema. Once the rules are created, the evaluation of the quality, performed through the integration with the QE, will use them to produce the quality metadata related to the dataset or the data element that will be evaluated.

The definition of the rules follows a formal representation based on the DDQV (DATAMITE DQV ) vocabulary extended from the DQV to cover the full expressivity of the rules as the UDRG presents them. The custom metrics are the central element from which the expressions can be formed, combining several simple expressions using logical operators. DDQV is an extension of DQV which is being developed within the context of this task.


  **Some key points of the tool are**
====================================================



*Creation of custom rules by the user.


*Custom rules evaluation to measure the dataset quality.


