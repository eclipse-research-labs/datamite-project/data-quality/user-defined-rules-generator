.. -------------------------------------------------------------------------------
..  MIT License
..
..  Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)
..
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
..
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
..
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
..
..  SPDX-License-Identifier: MIT
.. -------------------------------------------------------------------------------
Architecture & Design
=====

.. _diagram:

Architecture Diagram
------------

The following figure presents the high-level overview of the UDRG component.

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/raw/main/doc/figures/udrg_arch.svg


The front end will provide the end user an editor to create the rules. These rules will be created by selecting the available indicators, taken from the KPI Library, as well as the columns that the given dataset has in its schema. This editor will invoke the UDRG Rest API, which will create, modify or remove the rules in the rules database.

Once the rules are created, their evaluation will be carried out by the QE, which will invoke the UDRG Rules Interpreter through the provided UDRG Rest API.

.. _rulecreationseqdiag:

Rules creation sequence diagram
------------

The workflow and the elements involved in the process of creating a rule is reflected in the following sequence diagram.

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/raw/main/doc/figures/rules_creation_seq_diagram.png

The elements involved in the Rules creation process are the front-end or GUI, the Metadata repository (ATLAS), the KPI Library and the Rules Library (MongoDB of the UDRG).

The GUI is in charge of presenting to the user the elements to be selected (from a given dataset) as operands and operators so the final expression can be as complex as required. The storage of the rules is performed as a call to a method provided by the UDRG API.

The user can re-use rules as they are available for selection and modification. Some adjustments should be performed to follow the schema for the current dataset.

.. _ruleevalseqdiag:

Rules evaluation sequence diagram
------------

The workflow and the elements involved in the process of a rule evaluation is reflected in the following sequence diagram.

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/raw/main/doc/figures/rules_interp_seq_diagram.jpg

The interpretation of the rules is a process that will be called by the QE once the KPIs and the rules involved are informed by invoking a method provided by the UDRG API. So, the QE will call to the interpreter passing the values of the calculated KPIs that are needed for the rules, the dataset_id and the data to be evaluated as parameters.

.. _ddqvmodel:

DDQV model
------------
The definition of the rules follows a formal representation based on the DDQV (DATAMITE DQV ) vocabulary extended from the `DQV <https://www.w3.org/TR/vocab-dqv/>`_ vocabulary to cover the full expressivity of the rules implemented by the UDRG. The custom metrics are the central element from which the expressions can be formed, combining several simple expressions using logical operators.

The following figure presents the current DDQV model:

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/raw/main/doc/figures/qualityModel_thirdVersion.png
