.. -------------------------------------------------------------------------------
..  MIT License
..
..  Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)
..
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
..
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
..
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
..
..  SPDX-License-Identifier: MIT
.. -------------------------------------------------------------------------------
Installation & Setup
=====

.. _installationrestapi:

REST API of the User Defined Rules Generator
--------------------------------------------

The User Defined Rules Generator (UDRG) can be installed via docker
compose. The composition includes: - A Mongo database. - The UDRG with
its HTTP API.

The HTTP API offered by the UDRG is specified `here <https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/blob/main/doc/user-defined-rules-generator-openapi.yaml?ref_type=heads>`_.

Dataset and KPIs sample files to test the HTTP API can be found
`here <https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator/-/tree/main/app/examples/>`__.

Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~
To install the project, you need to have Docker and Docker-compose installed on your machine. If you
don’t have it, you can install it by following the instructions on the
official Docker website.

Get the code
~~~~~~~~~~~~~~~~~~~~~~~~~~
Clone this repository:

.. code:: bash

   git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-quality/user-defined-rules-generator.git

Configure properties files
~~~~~~~~~~~~~~~~~~~~~~~~~~

Check the ``user_defined_rules.env`` file and change the property values
as required.

Start containers
~~~~~~~~~~~~~~~~

Create and start the containers specified in the ``docker-compose.yml``
file:

.. code:: bash

   docker-compose up -d

.. _installationlib:

Rules interpreter as Python library
-----------------------------------

The user defined rules interpreter is also provided as a Python library.

Installation
~~~~~~~~~~~~

Generate the wheel file

.. code:: bash

   cd lib
   python setup.py bdist_wheel --universal

Install library:

.. code:: bash

   cd lib/dist
   pip3 install ruleinterpreter-0.1-py2.py3-none-any.whl

