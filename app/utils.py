"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
"""

"""
This module contains some utils functions used by the other modules.
"""
import io
import json
import os
import pandas as pd

from fastapi import UploadFile
from ruleinterpreter import custom_metrics_utils
from ruleinterpreter import models


def read_input_data_into_df(dataset_data: UploadFile, csv_sep=',', decimal_delimeter='.', dataset_encoding = 'utf-8'):
    """
    Convert input dataset data from CSV, JSON or Excel format into a Pandas dataframe.

    :param dataset_data: UploadFile parameter which contains the data.
    :type dataset_data: class:`fastapi.UploadFile`
    :param csv_sep: in case the input data is in CSV format, the delimiter character used.
    :type csv_sep: str, optional
    :param decimal_delimeter: character to recognize as decimal point. Default value is the point.
    :type decimal_delimeter: str, optional
    :param dataset_encoding: dataset encoding. Default value is UTF-8.
    :type dataset_encoding: str, optional
    :return: the data in Pandas dataframe format.
    :rtype: pandas.DataFrame
    """
    # Convert input dataset data into a Pandas dataframe
    file_name, file_extension = os.path.splitext(dataset_data.filename)
    if file_extension == '.csv':
        # Read CSV
        data_df = pd.read_csv(dataset_data.file, sep=csv_sep, encoding=dataset_encoding, decimal=decimal_delimeter)
    elif file_extension == '.json':
        dataset_data_json = json.load(dataset_data.file)
        # Convert input dataset data from JSON into pandas Dataframe
        data_df = pd.json_normalize(dataset_data_json)
    elif file_extension == '.xlsx':
        data_df = pd.read_excel(io.BytesIO(dataset_data.file.read()))

    return data_df


def rules_description(custom_metric: models.CustomMetric):
    """
    Generate the description, in text format, of the rules contained in a CustomMetric.

    :param custom_metric: the CustomMetric for which the description is going to be generated.
    :type custom_metric: class:`models.CustomMetric`
    :return: the description of the rules. E.g.: (columnValue[Humidity] >= 0.0) AND (columnValue[Humidity] <= 100.0)
        AND (numeric_mean[Humidity] >= 80.0).
    :rtype: str
    """
    expression_desc = ''
    for index, rule in enumerate(custom_metric.has_rule):
        expression_desc = expression_desc + '(' + operand_refers_to_description(rule.left_operand)
        expression_desc = expression_desc + ' ' + operator_description(rule.operator) + ' '
        if rule.right_operand.referring_operand:
            expression_desc = expression_desc + operand_refers_to_description(rule.right_operand.referring_operand)
        elif rule.right_operand.value_operand:
            expression_desc = expression_desc + operand_value_description(rule.right_operand.value_operand)
        expression_desc = expression_desc + ')'
        if index < (len(custom_metric.has_rule) - 1):
            # Add logical operator to expression
            if custom_metric.logical_operator == models.LogicalOperator.AND:
                expression_desc = expression_desc + ' AND '
            else:
                expression_desc = expression_desc + ' OR '

    return expression_desc


def operand_refers_to_description(rule_operand_refers_to: models.RuleOperandRefersTo):
    """
    Generate the description, in text format, of the rule operand of type "refers_to".

    :param rule_operand_refers_to: the "refers_to" operand for which the description is going to be generated.
    :type rule_operand_refers_to: class:`models.RuleOperandRefersTo`
    :return: the description of the "refers_to" operand. E.g.: columnValue[Humidity] , mean[Humidity].
    :rtype: str
    """
    expression_desc = ''
    if rule_operand_refers_to.refers_to == 'ddqv:columnValue':
        expression_desc = expression_desc + 'columnValue[' + rule_operand_refers_to.column_name + ']'
    else:
        expression_desc = expression_desc + rule_operand_refers_to.refers_to + \
                          '[' + rule_operand_refers_to.column_name + ']'
    #Check if opModificator
    if rule_operand_refers_to.op_modifier and len(rule_operand_refers_to.op_modifier.strip()) > 0:
        expression_desc = rule_operand_refers_to.op_modifier.replace('x', str(expression_desc))
    return expression_desc


def operand_value_description(rule_operand_value: models.RuleOperandValue):
    """
    Generate the description, in text format, of the rule operand of type "value".

    :param rule_operand_value: the "value" operand for which the description is going to be generated.
    :type rule_operand_value: class:`models.RuleOperandValue`
    :return: the description of the "value" operand. E.g.: 100.0.
    :rtype: str
    """
    expression_desc = ''
    expression_desc = str(custom_metrics_utils.get_rdf_value(rule_operand_value.operand_value))
    return expression_desc


def operator_description(operator: models.Operator):
    """
    Generate the description, in text format, of the rule operator.

    :param operator: the operator for which the description is going to be generated.
    :type operator: class:`models.Operator`
    :return: the description of the operator E.g.: <=.
    :rtype: str
    """
    operator_desc = ''
    if operator == models.Operator.EQ:
        operator_desc = '=='
    elif operator == models.Operator.NEQ:
        operator_desc = '!='
    elif operator == models.Operator.LT:
        operator_desc = '<'
    elif operator == models.Operator.GT:
        operator_desc = '>'
    elif operator == models.Operator.LTEQ:
        operator_desc = '<='
    elif operator == models.Operator.GTEQ:
        operator_desc = '>='
    elif operator == models.Operator.IN:
        operator_desc = 'IN'
    elif operator == models.Operator.REGEXMATCH:
        operator_desc = 'regexMatch'
    return operator_desc
