"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
"""

"""
This module contains the Open API functions.
"""
import json
import logging
import logging.config
import os
import pandas as pd
import pymongo
import uvicorn

from app import utils

from contextlib import asynccontextmanager
from fastapi import FastAPI, File, Form, HTTPException, Request, UploadFile, Body, status
from fastapi.encoders import jsonable_encoder
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from ruleinterpreter import models
from ruleinterpreter.rule_interpreter import RuleInterpreter
from ruleinterpreter.custom_metrics_utils import KPINotFoundError, CustomMetricNotFoundError
from ruleinterpreter.rules_db_utils import get_custom_metric_associated_to_dataset
from typing import Annotated, Any, Optional, List, Union

DEBUG = os.getenv('DEBUG') or False

tags_metadata = [
    {
        "name": "Custom metrics",
        "description": "Operations with custom metrics.",
    },
    {
        "name": "General",
        "description": "Generic functions.",
    },
    {
        "name": "KPI Library",
        "description": "KPI Library configuration",
    },
    {
        "name": "Data Model",
        "description": "Data Model access",
    },
]


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Code that will be executed once, before the application starts receiving requests.
    It will get some environment variables values, get the connection to the Mongo DB where the custom metrics are
    stored, and it will configure the application logging.

    :param app: the FastAPI app object.
    :type app: class:`fastapi.FastAPI`
    """
    # Get DB connection
    rules_db_hostname = os.getenv("DB_HOSTNAME", "datamite")
    rules_db_port = os.getenv("DB_PORT", "27017")
    rules_db_username = os.getenv("DB_USERNAME", "root")
    rules_db_password = os.getenv("DB_PASSWORD", "datamite")
    rules_db_dbname = os.getenv("DB_NAME", "datamite-quality-rules")
    connection_string = f"mongodb://{rules_db_username}:{rules_db_password}@{rules_db_hostname}:{rules_db_port}"
    app.mongodb_client = pymongo.MongoClient(connection_string)
    app.database = app.mongodb_client[rules_db_dbname]

    # Configure logs
    logger = logging.getLogger("uvicorn.error")
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(asctime)s | %(levelname)s | %(module)s | %(message)s"))
    logger.addHandler(handler)
    logging_level = logging.INFO
    if DEBUG:
        logging_level = logging.DEBUG
    logger.setLevel(logging_level)
    logger.propagate = False

    yield
    # Close DB connection
    app.mongodb_client.close()


app = FastAPI(lifespan=lifespan, title="DATAMITE User Defined Quality Rules API",
              description=f"User Defined Quality Rules. "
                          f"API documentation", openapi_tags=tags_metadata)
# Enable CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logger = logging.getLogger("uvicorn.error")


@app.get("/")
async def root() -> Any:
    """
    Testing function.
    """
    return {"message": "Hello, I'm DATAMITE User Defined Quality Rules!"}


####################################
# Custom Metrics API
####################################
@app.get("/quality/customMetric", summary="Get all custom metrics", tags=["Custom metrics"],
         response_description="All custom metrics",
         response_model=List[models.DatasetCustomMetrics],
         response_model_exclude_none=True,
         response_model_by_alias=True
         )
async def get_custom_metrics(request: Request):
    """
    Get all the available custom metrics.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :return: the list of custom metrics.
    :rtype: list of class:`models.DatasetCustomMetrics` in Json format
    """
    # Examples in https://www.mongodb.com/developer/languages/python/python-quickstart-fastapi/#prerequisites
    # Get custom metrics from Rules repo
    custom_metrics_list = list(request.app.database["qualityRules"].find(limit=100))
    return custom_metrics_list


@app.get("/quality/customMetric/{dataset_id}", summary="Get custom metrics associated to a dataset", tags=["Custom metrics"],
         response_description="Custom metrics of a specific dataset",
         response_model=models.DatasetCustomMetrics,
         response_model_exclude_none=True,
         response_model_by_alias=True
         )
async def get_dataset_custom_metrics(request: Request, dataset_id: str):
    """
    Get the custom metrics associated to a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :return: the list of custom metrics associated to the specified dataset.
    :rtype: list of class:`models.DatasetCustomMetrics` in Json format
    """
    # Get custom metrics from Rules repo
    if (
            # custom_metrics = request.app.database["qualityRules"].find_one({"dataset_id": dataset_id})
            custom_metrics := request.app.database.get_collection("qualityRules").find_one({"dataset_id": dataset_id})
    ) is not None:
        return custom_metrics

    raise HTTPException(status_code=404, detail=f"Custom metrics for dataset {dataset_id} not found")


@app.get("/quality/customMetric/{dataset_id}/cm", summary="Get a specific custom metric associated to a dataset", tags=["Custom metrics"],
         response_description="Custom metric of a specific dataset",
         response_model=models.CustomMetric,
         response_model_exclude_none=True,
         response_model_by_alias=True
         )
async def get_dataset_custom_metric(request: Request, dataset_id: str, custom_metric_id: str):
    """
    Get a specific custom metric associated to a specific dataset.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param custom_metric_id: the custom metric identifier.
    :type custom_metric_id: str
    :return: the custom metric.
    :rtype: list of class:`models.CustomMetric` in Json format
    """
    # Get a specific custom metric from Rules repo
    results = get_custom_metric_associated_to_dataset(request.app.database, dataset_id, custom_metric_id)
    dataset_custom_metric_db = None
    for ds in results:
        dataset_custom_metric_db = ds["ddqv_hasCustomMetric"]

    if dataset_custom_metric_db is not None:
        return dataset_custom_metric_db

    raise HTTPException(status_code=404, detail=f"Custom metric {custom_metric_id} for dataset {dataset_id} not found")


@app.post("/quality/customMetric/{dataset_id}", summary="Add a custom metric for a dataset", tags=["Custom metrics"],
          response_description="Created custom metrics",
          response_model=models.DatasetCustomMetrics,
          response_model_exclude_none=True,
          status_code=status.HTTP_201_CREATED,
          response_model_by_alias=True
          )
async def create_custom_metric(request: Request, dataset_id: str, new_custom_metric: models.NewCustomMetric):
    """
    Add a custom metric for a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param new_custom_metric: the custom metric to add.
    :type new_custom_metric:  class:`models.NewCustomMetric`
    :return: the added custom metric.
    :rtype: class:`models.CustomMetric` in Json format
    """
    #  TODO: Check new custom metric is correct. E.g.: kpi is correct for column type (numeric, tex, etc.)
    # Create a CustomMetric object from an existing NewCustomMetric object, by adding the id
    custom_metric = models.CustomMetric.from_new_custom_metric(new_custom_metric)
    # Check if dataset_id already exists
    if (custom_metrics := request.app.database.get_collection("qualityRules").find_one({"dataset_id": dataset_id})) \
            is None:
        # Dataset_id does not exist
        # Insert the dataset together with the new custom metric
        custom_metrics_list = list()
        custom_metrics_list.append(custom_metric)
        dataset_custom_metric = models.DatasetCustomMetrics(dataset_id=dataset_id, custom_metrics=custom_metrics_list)
        created_custom_metric = request.app.database.get_collection("qualityRules").insert_one(
            dataset_custom_metric.model_dump(by_alias=True, exclude=["id"])
        )
        created_custom_metric_id = created_custom_metric.inserted_id
    else:
        # Dataset_id already exists
        # Add new custom metric to existing dataset
        created_custom_metric = request.app.database.get_collection("qualityRules").find_one_and_update(
            {"dataset_id": dataset_id},
            {"$push": {"ddqv_hasCustomMetric": custom_metric.model_dump(by_alias=True)}})
        created_custom_metric_id = created_custom_metric["_id"]

    # Return created custom metric
    return_custom_metric = request.app.database.get_collection("qualityRules").find_one({"_id": created_custom_metric_id})
    return return_custom_metric


@app.put("/quality/customMetric/{dataset_id}", summary="Update a custom metric for a dataset", tags=["Custom metrics"],
          response_description="Updated custom metrics",
          response_model=models.DatasetCustomMetrics,
          response_model_exclude_none=True,
          status_code=status.HTTP_200_OK,
          response_model_by_alias=True
          )
async def update_custom_metric(request: Request, dataset_id: str, custom_metric: models.CustomMetric):
    """
    Update a custom metric for a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param custom_metric: the custom metric to update.
    :type custom_metric:  class:`models.CustomMetric`
    :return: the updated custom metric.
    :rtype: class:`models.CustomMetric` in Json format
    """
    #  TODO: Check new custom metric is correct. E.g.: kpi is correct for column type (numeric, tex, etc.)
    # Check if dataset_id already has custom_metrics
    if (custom_metrics := request.app.database.get_collection("qualityRules").find_one({"dataset_id": dataset_id})) \
            is None:
        # Insert the dataset together with the new custom metric
        custom_metrics_list = list()
        custom_metrics_list.append(custom_metric)
        dataset_custom_metric = models.DatasetCustomMetrics(dataset_id=dataset_id, custom_metrics=custom_metrics_list)
        updated_custom_metric = request.app.database.get_collection("qualityRules").insert_one(
            dataset_custom_metric.model_dump(by_alias=True, exclude=["id"])
        )
        updated_custom_metric_id = updated_custom_metric.inserted_id
    # Check if dataset_id already has the specified custom_metric
    elif (custom_metrics := request.app.database.get_collection("qualityRules").find_one({"dataset_id": dataset_id,
            "ddqv_hasCustomMetric.dcterms_identifier": custom_metric.id})) is None:
        # Add new custom metric to existing dataset
        updated_custom_metric = request.app.database.get_collection("qualityRules").find_one_and_update(
            {"dataset_id": dataset_id},
            {"$push": {"ddqv_hasCustomMetric": custom_metric.model_dump(by_alias=True)}})
        updated_custom_metric_id = updated_custom_metric["_id"]
    else:
        # Update the already existing custom metric
        updated_custom_metric = request.app.database.get_collection("qualityRules").find_one_and_update(
            {"dataset_id": dataset_id, "ddqv_hasCustomMetric.dcterms_identifier": custom_metric.id},
            {"$set": {"ddqv_hasCustomMetric.$": custom_metric.model_dump(by_alias=True)}})
        updated_custom_metric_id = updated_custom_metric["_id"]

    # Return created/updated custom metric
    return_custom_metric = request.app.database.get_collection("qualityRules").find_one({"_id": updated_custom_metric_id})
    return return_custom_metric


@app.delete("/quality/customMetric/{dataset_id}", summary="Delete a custom metric of a dataset", tags=["Custom metrics"],
            response_description="Delete custom metric",
            status_code=status.HTTP_204_NO_CONTENT
            )
async def delete_custom_metric(request: Request, dataset_id: str, custom_metric_id: str):
    """
    Delete a specific custom metric for a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param custom_metric_id: the custom metric identifier.
    :type custom_metric_id: str
    """
    updated_custom_metric = request.app.database.get_collection("qualityRules").find_one_and_update(
        {"dataset_id": dataset_id},
        {"$pull": {'ddqv_hasCustomMetric':{"dcterms_identifier": custom_metric_id}}})
    return


@app.delete("/quality/customMetric/{dataset_id}/all", summary="Delete all custom metrics of a dataset",
            tags=["Custom metrics"],
            response_description="Delete all custom metrics",
            status_code=status.HTTP_204_NO_CONTENT
            )
async def delete_all_custom_metrics(request: Request, dataset_id: str):
    """
    Delete all custom metrics associated to a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    """
    updated_custom_metric = request.app.database.get_collection("qualityRules").delete_many({"dataset_id": dataset_id})
    return


@app.get("/quality/customMetric/active/{dataset_id}", summary="Get active status of custom metrics associated to a dataset",
         tags=["Custom metrics"],
         response_description="Active status of associated custom metrics",
         response_model = List[models.DatasetCustomMetricActiveStatus]
         )
async def get_dataset_custom_metrics_active_status(request: Request, dataset_id: str):
    """
    Get the active status of the custom metrics associated to a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :return: the list of active status of the custom metrics associated to the specified dataset.
    :rtype: list of class:`models.DatasetCustomMetricActiveStatus` in Json format
    """
    custom_metrics_active_status = []
    # Get custom metrics from Rules repo
    dataset_custom_metrics_db = request.app.database.get_collection("qualityRules").find_one({"dataset_id": dataset_id})
    if dataset_custom_metrics_db:
        custom_metrics = models.DatasetCustomMetrics(**dataset_custom_metrics_db)
        for custom_metric in custom_metrics.custom_metrics:
            custom_metrics_active_status.append(models.DatasetCustomMetricActiveStatus(id=custom_metric.id,
                                                                              is_active=custom_metric.is_active))

    return custom_metrics_active_status


@app.post("/quality/customMetric/active/{dataset_id}", summary="Update active status of custom metrics associated to a dataset",
          tags=["Custom metrics"],
          status_code=status.HTTP_201_CREATED,
          response_description="Active status of associated custom metrics",
          response_model=List[models.DatasetCustomMetricActiveStatus]
          )
async def update_dataset_custom_metrics_active_status(request: Request, dataset_id: str,
                                            custom_metrics_active_status: List[models.DatasetCustomMetricActiveStatus]):
    """
    Update the active status of the custom metrics associated to a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param custom_metrics_active_status: list of active status of the custom metrics.
    :type custom_metrics_active_status: list of class:`models.DatasetCustomMetricActiveStatus` in Json format
    :return: the list of active status of the custom metrics associated to the specified dataset.
    :rtype: list of class:`models.DatasetCustomMetricActiveStatus` in Json format
    """
    for custom_metric_active_status in custom_metrics_active_status:
        # Update the active status of already existing custom metric
        updated_custom_metric = request.app.database.get_collection("qualityRules").find_one_and_update(
            {"dataset_id": dataset_id},
            {"$set": {"ddqv_hasCustomMetric.$[metricItem].is_active": custom_metric_active_status.is_active}},
            array_filters=[{"metricItem.dcterms_identifier": custom_metric_active_status.id}]
        )
    return custom_metrics_active_status


@app.get("/quality/customMetric/activeDataset/{dataset_id}", summary="Check if specified dataset has active custom metrics associated",
         tags=["Custom metrics"],
         response_description="True/False"
         )
async def check_dataset_active_custom_metrics(request: Request, dataset_id: str):
    """
    Check if specified dataset has active custom metrics associated to it, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :return: True/False, if dataset has or no active custom metrics associated to it.
    :rtype: boolean
    """
    associated_active_custom_metrics= dict()
    # Check if dataset has active custom metrics in Rules repo
    check_active_custom_metrics = RuleInterpreter.check_dataset_active_custom_metrics_static(request.app.database, dataset_id)
    associated_active_custom_metrics["dataset_id"] = dataset_id
    associated_active_custom_metrics["active"] = check_active_custom_metrics
    return associated_active_custom_metrics


@app.get("/quality/customMetric/kpis/required", summary="Get the required KPIs for the custom metrics", tags=["Custom metrics"],
         response_description="Required KPIs"
         )
async def get_custom_metrics_req_kpis(request: Request, dataset_id: str, custom_metric_id: str = None) -> list:
    """
    Get the names of the KPI-s required by the custom metrics associated to a specific dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param custom_metric_id: the custom metric identifier.
    :type custom_metric_id: str
    :return: the list of the names of the required KPI-s.
    :rtype: list of str
    """
    kpis = []
    logger.info(f'dataset_id={dataset_id}')
    custom_metrics_list = None
    if custom_metric_id is not None:
        custom_metrics_list = [custom_metric_id]
    kpis = RuleInterpreter.get_custom_metrics_req_kpis_static(request.app.database, dataset_id, custom_metrics_list)
    return kpis


@app.get("/quality/customMetric/cols/required", summary="Get the required columns for the custom metrics",
         tags=["Custom metrics"],
         response_description="Required columns"
         )
async def get_custom_metrics_req_cols(request: Request, dataset_id: str, custom_metric_id: str = None) -> list:
    """
    Get the names of the dataset columns required by the custom metrics associated to a specific dataset, looked up by
    `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param custom_metric_id: the custom metric identifier.
    :type custom_metric_id: str
    :return: the list of the names of the required dataset columns.
    :rtype: list of str
    """
    cols = []
    logger.info(f'dataset_id={dataset_id}')
    custom_metrics_list = None
    if custom_metric_id is not None:
        custom_metrics_list = [custom_metric_id]
    cols = RuleInterpreter.get_custom_metrics_req_cols_static(request.app.database, dataset_id, custom_metrics_list)
    return cols


@app.post("/quality/customMetric/dimension/proposed", summary="Suggest a dimension for a custom metric", tags=["Custom metrics"])
async def get_custom_metric_dimension(custom_metric: models.CustomMetric) -> list:
    """
    Recommended quality dimensions for the specified custom metric.

    :param custom_metric: the custom metric for which to recommend quality dimensions.
    :type custom_metric: class:`models.CustomMetric`
    :return: the list of recommended dimensions.
    :rtype: list of str
    """
    logger.info(f'custom_metric={custom_metric}')
    # TODO: Determine custom metric dimension
    dimensions = ['ACCURACY', 'VALIDITY']
    return dimensions


@app.post("/quality/customMetric/interpreter/compute", summary="Evaluate the custom metrics associated to a dataset",
          tags=["Custom metrics"],
          response_description="Quality measurements",
          response_model=list[models.QualityMeasurement],
          response_model_by_alias=True
          )
async def custom_metrics_interpreter(request: Request, dataset_id: str,
                        dataset_data: Annotated[UploadFile, File(description="Dataset")],
                        csv_separator: str = ',',
                        decimal_delimeter: str = '.',
                        dataset_encoding: str = 'utf-8',
                        dataset_kpis: Annotated[UploadFile, File(description="KPIs")] = None,
                        custom_metric_id: str = None):
    """
    Evaluate the custom metrics associated to a dataset, looked up by `dataset_id`.

    :param request: the HTTP request object.
    :type request: class:`fastapi.Request`
    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :param dataset_data: the dataset data.
    :type dataset_data: class:`fastapi.UploadFile`
    :param csv_separator: the CSV character separator, if data in CSV format. Default value is the comma.
    :type csv_separator: str
    :param decimal_delimeter: character to recognize as decimal point. Default value is the point.
    :type decimal_delimeter: str
    :param dataset_encoding: dataset encoding. Default value is UTF-8.
    :type dataset_encoding: str
    :param dataset_kpis: the current values of the required dataset KPI-s.
    :type dataset_kpis: class:`fastapi.UploadFile`
    :param custom_metric_id: the custom metric identifier.
    :type custom_metric_id: str
    :return: the list of evaluation results of the evaluated custom metrics.
    :rtype: list of class:`models.QualityMeasurement` in Json format
    """
    logger.info(f'dataset_id={dataset_id}')
    custom_metrics_list = None
    if custom_metric_id is not None:
        custom_metrics_list = [custom_metric_id]
    # Get dataset data
    if not dataset_data:
        raise HTTPException(status_code=400, detail="dataset_data: No file part in the request")
    if dataset_data.filename == '':
        raise HTTPException(status_code=400, detail="dataset_data: No file selected for uploading")
    #
    # Convert input dataset data from CSV into pandas Dataframe
    try:
        data_df = utils.read_input_data_into_df(dataset_data, csv_separator, decimal_delimeter, dataset_encoding)
    except:
        raise HTTPException(status_code=400, detail="dataset_data: Invalid input format. Accepted extensions: .csv, .json and .xlsx")

    # Get KPIs
    kpis_df = pd.DataFrame()
    if dataset_kpis:
        if dataset_kpis.filename == '':
            raise HTTPException(status_code=400, detail="dataset_kpis: No file selected for uploading")
        else:
            dataset_kpis_json = json.load(dataset_kpis.file)
            # Convert input kpis from JSON into pandas Dataframe
            kpis_df = pd.json_normalize(dataset_kpis_json)

    # Rule interpreter
    try:
        measurements_list = RuleInterpreter.compute_custom_metrics_static(request.app.database, dataset_id, data_df,
                                                                          kpis_df, custom_metrics_list)
        json_measurements_list = jsonable_encoder(measurements_list)
        return JSONResponse(content=json_measurements_list)
    except KeyError as ke:
        raise HTTPException(status_code=400, detail=f"Item {ke} not found")
    except KPINotFoundError as kpi_error:
        raise HTTPException(status_code=400, detail=f"{kpi_error}")


@app.post("/quality/customMetric/rules/description", summary="Get description of a custom metric", tags=["Custom metrics"])
async def get_custom_metric_description(custom_metric: models.CustomMetric):
    """
    Get the description, in text format, of the rules included in a custom metric.

    :param custom_metric: the custom metric object.
    :type custom_metric: class:`models.CustomMetric`
    :return: the description of the rules in text format.
    :rtype: str
    """
    custom_metric_description = utils.rules_description(custom_metric)
    return custom_metric_description


####################################
# General API
####################################
@app.post("/quality/level", summary="Calculate quality level for the specified dimension", tags=["General"])
async def dimension_quality_level(dimension: str, threshold: float, custom_metrics: list[models.CustomMetric],
                                  quality_measurements: list[models.QualityMeasurement]):
    """
    Calculate quality level for the specified dimension.

    :param dimension: the quality dimension.
    :type dimension: str
    :param threshold: the threshold to consider to determine the quality level.
    :type threshold: float
    :param custom_metrics: the custom metrics to consider.
    :type custom_metrics: list of class:`models.CustomMetric`
    :param quality_measurements: the evaluation result of the custom metrics to consider.
    :type quality_measurements: list of class:`models.QualityMeasurement`
    :return: the quality level in % and text format.
    :rtype: str
    """
    logger.info(f'dimension={dimension}')
    logger.info(f'threshold={threshold}')
    logger.info(f'custom_metrics={custom_metrics}')
    logger.info(f'quality_measurements={quality_measurements}')
    # TODO: Calculate quality level for the specified dimension
    result = {"quality_fulfilment": "98%", "quality_level": "Medium"}
    return result


@app.post("/quality/rdf_format", summary="Convert custom metrics into RDF format", tags=["General"])
async def convert_to_rdf(custom_metrics: list[models.CustomMetric], quality_measurements: list[models.QualityMeasurement]):
    """
    Convert quality related data into RDF format.

    :param custom_metrics: the custom metrics.
    :type custom_metrics: list of class:`models.CustomMetric`
    :param quality_measurements: the evaluation result of the custom metrics.
    :type quality_measurements: list of class:`models.QualityMeasurement`
    :return: the quality data in RDF format.
    :rtype: str
    """
    logger.info(f'custom_metrics={custom_metrics}')
    logger.info(f'quality_measurements={quality_measurements}')
    # TODO: Convert quality related data into RDF format
    return 'RDF_content'


####################################
# KPI Library API
####################################
@app.get("/quality/kpilibrary/kpis", summary="Get all kpis", tags=["KPI Library"])
async def get_kpis() -> list:
    """
    Get all the available KPI-s.

    :return: the available KPI-s.
    :rtype: list of str
    """
    kpis_list = []
    # TODO: Get KPI-s of KPI library
    # Opening KPIs file
    document_path = os.path.abspath(os.path.dirname(__file__)) + '/./conf/KPIs_list.txt'
    with open(document_path) as file:
        kpis_list = [line.rstrip() for line in file]
    return kpis_list


####################################
# ATLAS API
####################################
@app.get("/datamodel/dataset/columns", summary="Get dataset columns", tags=["Data Model"])
async def get_dataset_columns(dataset_id: str) -> list:
    """
    Get dataset columns, looked up by `dataset_id`.

    :param dataset_id: the dataset identifier.
    :type dataset_id: str
    :return: the dataset columns.
    :rtype: list of str
    """
    # TODO: Get dataset columns from ATLAS
    cols_list = ["TempMin", "Humidity", "DateTime", "TempMax", "Temperature"]
    cols_list = ["Num1","Num2","Cat1","Cat2","Date","Text"]
    cols_list = ["Min Usage Level [%]","Max Usage Level [%]","Installed Power [kVA]","Latitude","Longitude"]
    return cols_list


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=9102)
