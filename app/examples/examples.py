import pandas as pd

#from app import models
from ruleinterpreter import models
from pydantic import parse_obj_as
from typing import List

ddqv_custom_metric1 = {
			"is_active": True,
			"weight": 0.23,
			"dcterms_identifier":"https://doi.org/Dataset/meteo/CustomMetric/tempValueRange",
			"rdf_type": "ddqv:CustomMetric",
			"dcterms_title":"Temperature accepted range values.",
			"skos_definition":"The temperature column should be between -50 and 50.",
			"dqv_expectedDataType": "xsd:double",
			"dqv_inDimension": "ddqv:accuracy",
			"ddqv_logicalOperator": "ddqv:and",
			"prov_wasGeneratedBy": "TECNALIA",
				"ddqv_hasRule": [
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"-50.0\"^^xsd:double"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:lteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"50.0\"^^xsd:double"
						}
					}
				}
			]
		}


ddqv_custom_metric2 = 		{
			"is_active": True,
			"weight": 0.51,
			"dcterms_identifier":"https://doi.org/Dataset/meteo/CustomMetric/tempMeanRange",
			"rdf_type": "ddqv:CustomMetric",
			"dcterms_title":"Temperature mean accepted range values.",
			"skos_definition":"The temperature column mean value should be greather or equal than 15.",
			"dqv_expectedDataType": "xsd:boolean",
			"dqv_inDimension": "ddqv:accuracy",
			"ddqv_logicalOperator": "ddqv:and",
			"prov_wasGeneratedBy": "TECNALIA",
			"ddqv_hasRule":	[{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "numeric_mean"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"1500\"^^xsd:double"
						}
					}
			}]
		}


ddqv_custom_metric3 = {
			"is_active": True,
			"weight": 0.13,
			"dcterms_identifier":"https://doi.org/Dataset/meteo/CustomMetric/Ex1",
			"rdf_type": "ddqv:CustomMetric",
			"dcterms_title":"Accepted range values and mean value for humidity.",
			"skos_definition":"The humidity column value should be between 0 and 100 and mean >= 80.",
			"dqv_expectedDataType": "xsd:double",
			"dqv_inDimension": "ddqv:accuracy",
			"ddqv_logicalOperator": "ddqv:and",
			"prov_wasGeneratedBy": "TECNALIA",
				"ddqv_hasRule": [
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Humidity",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"0.0\"^^xsd:double"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Humidity",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:lteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"100.0\"^^xsd:double"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Humidity",
							"ddqv_refersTo": "numeric_mean"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"80.0\"^^xsd:double"
						}
					}
				}
			]
		}


ddqv_custom_metric4 = {
			"is_active": True,
			"weight": 0.42,
			"dcterms_identifier":"https://doi.org/Dataset/meteo/CustomMetric/Ex2",
			"rdf_type": "ddqv:CustomMetric",
			"dcterms_title":"Accepted range values and mean value for humidity.",
			"skos_definition":"The humidity column value should be between <=10 and >=60 and mean >= 80",
			"dqv_expectedDataType": "xsd:double",
			"dqv_inDimension": "ddqv:accuracy",
			"ddqv_logicalOperator": "ddqv:or",
			"prov_wasGeneratedBy": "TECNALIA",
				"ddqv_hasRule": [
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Humidity",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"60.0\"^^xsd:double"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Humidity",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:lteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"10.0\"^^xsd:double"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Humidity",
							"ddqv_refersTo": "numeric_mean"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"80.0\"^^xsd:double"
						}
					}
				}
			]
		}

ddqv_custom_metric5 = {
			"is_active": True,
			"weight": 0.62,
			"dcterms_identifier":"https://doi.org/Dataset/meteo/CustomMetric/Ex3",
			"rdf_type": "ddqv:CustomMetric",
			"dcterms_title":"Temperature, TempMax, TempMin  accepted range values.",
			"skos_definition":"The Temperature, TempMax and TempMin column values should be between ..",
			"dqv_expectedDataType": "xsd:double",
			"dqv_inDimension": "ddqv:accuracy",
			"ddqv_logicalOperator": "ddqv:and",
			"prov_wasGeneratedBy": "TECNALIA",
				"ddqv_hasRule": [
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"value_operand": {
							"ddqv_operandValue": "\"-50.0\"^^xsd:double"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:lteq",
					"ddqv_rightOperand": {
						"referring_operand": {
							"ddqv_columnName": "TempMax",
							"ddqv_refersTo": "ddqv:columnValue"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"referring_operand": {
							"ddqv_columnName": "TempMin",
							"ddqv_refersTo": "ddqv:columnValue"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
					},
					"ddqv_operator": "ddqv:lteq",
					"ddqv_rightOperand": {
						"referring_operand": {
							"ddqv_columnName": "TempMax",
							"ddqv_refersTo": "numeric_mean"
						}
					}
				},
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "TempMax",
							"ddqv_refersTo": "numeric_mean"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"referring_operand": {
							"ddqv_columnName": "TempMin",
							"ddqv_refersTo": "numeric_mean"
						}
					}
				}
			]
		}


ddqv_custom_metric6 = {
			"is_active": True,
			"weight": 0.75,
			"dcterms_identifier":"https://doi.org/Dataset/meteo/CustomMetric/Ex4",
			"rdf_type": "ddqv:CustomMetric",
			"dcterms_title":"KPI mean <= columnValue",
			"skos_definition":"KPI mean TempMax <= Temperature columnValue.",
			"dqv_expectedDataType": "xsd:double",
			"dqv_inDimension": "ddqv:accuracy",
			"ddqv_logicalOperator": "ddqv:and",
			"prov_wasGeneratedBy": "TECNALIA",
				"ddqv_hasRule": [
				{
					"rdf_type": "ddqv:Rule",
					"ddqv_leftOperand": {
							"ddqv_columnName": "TempMax",
							"ddqv_refersTo": "numeric_mean"
					},
					"ddqv_operator": "ddqv:gteq",
					"ddqv_rightOperand": {
						"referring_operand": {
							"ddqv_columnName": "Temperature",
							"ddqv_refersTo": "ddqv:columnValue"
						}
					}
				}
			]
		}


def get_dataset_custom_metrics(dataset_id: str):
	# Create custom metric associated to dataset X. Custom metrics are associated to dataset
	left_op = models.RuleOperand(referring_operand=models.RuleOperandRefersTo(column_name='col1', refers_to='mean'))
	left_op_json = left_op.json()
	right_op = models.RuleOperand(value_operand=models.RuleOperandValue(operand_value='55'))
	right_op_json = right_op.json()

	custom_metrics_list_1 = list()
	custom_metrics_list_1.append(models.CustomMetric(**ddqv_custom_metric6))
	custom_metrics_list_1.append(models.CustomMetric(**ddqv_custom_metric1))
	custom_metrics_list_1.append(models.CustomMetric(**ddqv_custom_metric2))
	custom_metrics_list_1.append(models.CustomMetric(**ddqv_custom_metric3))
	custom_metrics_list_1.append(models.CustomMetric(**ddqv_custom_metric4))
	custom_metrics_list_1.append(models.CustomMetric(**ddqv_custom_metric5))
	#dataset_custom_metrics_list_1_m = parse_obj_as(List[models.DatasetCustomMetrics], dataset_custom_metrics_list_1)
	dataset_custom_metrics = models.DatasetCustomMetrics(dataset_id=dataset_id, custom_metrics=custom_metrics_list_1)

	return dataset_custom_metrics


def get_all_datasets_custom_metrics():
	list_dataset_custom_metrics = list()
	dataset_custom_metrics_1 = get_dataset_custom_metrics('dataset1')
	list_dataset_custom_metrics.append(dataset_custom_metrics_1)
	dataset_custom_metrics_2 = get_dataset_custom_metrics('dataset2')
	list_dataset_custom_metrics.append(dataset_custom_metrics_2)
	return list_dataset_custom_metrics


def get_dataset():
	# Create dataset
	# initialize list of lists
	data = [['Bilbao', -10, 120, 35, 0], ['Madrid', 45, 70, 40, -5], ['Barcelona', 14, 60, 45, 1], ['London', 5, 50, 30, -10], ['Paris', 4, 130, 38, -12]]
	# Create the pandas DataFrame
	data_df = pd.DataFrame(data, columns=['City', 'Temperature', 'Humidity', 'TempMax', 'TempMin'])

	data_df_json = data_df.to_json(orient="records")
	return data_df

