"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
"""

"""
This module contains the functions used to evaluate the rules inside the custom metrics.
"""
import logging
import pandas as pd
import pymongo

from ruleinterpreter import models, custom_metrics_utils, rules_db_utils
from datetime import datetime


logger = logging.getLogger(__name__)


class RuleInterpreter:
    """
    This class represents the rules interpreter. It will interpret the rules included in the custom metrics stored in a
    Mongo DB and that are associated to the specified dataset.

    :param rules_db_hostname: the Mongo DB hostname.
    :type rules_db_hostname: str
    :param rules_db_port: the Mongo DB port.
    :type rules_db_port: str
    :param rules_db_username: the Mongo DB user name.
    :type rules_db_username: str
    :param rules_db_password: the Mongo DB password.
    :type rules_db_password: str
    :param rules_db_dbname: the Mongo DB name.
    :type rules_db_dbname: str
    """
    def __init__(self, rules_db_hostname: str, rules_db_port: str, rules_db_username: str, rules_db_password: str,
                 rules_db_dbname: str):
        """
        Constructor method
        """
        self.rules_db_dbname = rules_db_dbname
        self.rules_connection_string = f"mongodb://{rules_db_username}:{rules_db_password}@{rules_db_hostname}:{rules_db_port}"

    def compute_custom_metrics(self, dataset_id: str, data_df: pd.DataFrame, kpis_df: pd.DataFrame,
                               custom_metrics_list: list = None):
        """
        Evaluate the list of rules included in the custom metrics stored in a Mongo DB and that are associated to the
        specified dataset. First of all, it gets the connection to the Mongo DB and then invokes to the corresponding
        method to do the actual evaluation.

        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement` in Json format
        """
        # Get DB connection
        mongodb_client = pymongo.MongoClient(self.rules_connection_string)
        mongodb_database = mongodb_client[self.rules_db_dbname]
        measurements_list = RuleInterpreter.compute_custom_metrics_static(mongodb_database, dataset_id, data_df, kpis_df,
                                                                          custom_metrics_list)
        # Convert from Pydantic model to JSON
        measurements_list_json = [ob.__dict__ for ob in measurements_list]
        # Close DB connection
        mongodb_client.close()
        return measurements_list_json

    @staticmethod
    def compute_custom_metrics_static(db: pymongo.database.Database, dataset_id: str, data_df: pd.DataFrame,
                                      kpis_df: pd.DataFrame, custom_metrics_list: list = None):
        """
        Evaluate the list of rules included in the custom metrics stored in a Mongo DB and that are associated to the
        specified dataset. First of all, it gets the connection to the Mongo DB and then invokes to the corresponding
        method to do the actual evaluation.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement` in Json format
        """
        if not custom_metrics_list:
            return RuleInterpreter.compute_custom_metrics_dataset(db, dataset_id, data_df, kpis_df,)
        else:
            return RuleInterpreter.compute_custom_metrics_dataset_cm(db, dataset_id, data_df, kpis_df, custom_metrics_list)

    @staticmethod
    def compute_custom_metrics_dataset(db: pymongo.database.Database, dataset_id: str, data_df: pd.DataFrame, kpis_df: pd.DataFrame):
        """
        Evaluate the list of rules included in the custom metrics stored in a Mongo DB and that are associated to the
        specified dataset. First of all, it retrieves the custom metrics associated to the specified dataset and for
        each of the active custom metrics, evaluates the rules inside it.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement`
        """
        measurements_list = list()
        # Get custom metrics associated to dataset
        dataset_custom_metrics_db = rules_db_utils.get_active_custom_metrics_associated_to_dataset(db, dataset_id)
        if dataset_custom_metrics_db is not None:
            dataset_custom_metrics = models.DatasetCustomMetrics(**dataset_custom_metrics_db)
            now = datetime.now()
            date_time = now.strftime("%Y-%m-%dT%H:%M:%SZ")
            for custom_metric in dataset_custom_metrics.custom_metrics:
                if custom_metric.is_active:
                    # Only compute active rules
                    evaluation_result = custom_metrics_utils.custom_metric_interpreter(data_df, custom_metric, kpis_df)

                    measurement = models.QualityMeasurement(dqv_isMeasurementOf=custom_metric.id,
                                                            dqv_computedOn=dataset_id, rdf_datatype='xsd:decimal',
                                                            ddqv_hasParameters=[],
                                                            dqv_value=evaluation_result,
                                                            dcterms_title=custom_metric.title)
                    measurements_list.append(measurement)
                    logger.debug(f'evaluation_result of ddqv_custom_metric={evaluation_result}')
        return measurements_list

    @staticmethod
    def compute_custom_metrics_dataset_cm(db: pymongo.database.Database, dataset_id: str, data_df: pd.DataFrame,
                                          kpis_df: pd.DataFrame, custom_metrics_list: list):
        """
        Evaluate the list of rules included in the specified custom metrics stored in a Mongo DB and that are associated to the
        specified dataset. First of all, it retrieves the specified custom metric associated to the specified dataset
        and evaluates the rules inside it.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement`
        """
        measurements_list = list()
        if custom_metrics_list is not None:
            now = datetime.now()
            date_time = now.strftime("%Y-%m-%dT%H:%M:%SZ")
            for custom_metric_id in custom_metrics_list:
                # Get specified custom metric associated to dataset
                results = rules_db_utils.get_custom_metric_associated_to_dataset(db, dataset_id, custom_metric_id)
                dataset_custom_metric_db = None
                for ds in results:
                    dataset_custom_metric_db = ds["ddqv_hasCustomMetric"]

                if dataset_custom_metric_db is not None:
                    dataset_custom_metric = models.CustomMetric(**dataset_custom_metric_db)
                    evaluation_result = custom_metrics_utils.custom_metric_interpreter(data_df, dataset_custom_metric,
                                                                                       kpis_df)

                    measurement = models.QualityMeasurement(dqv_isMeasurementOf=dataset_custom_metric.id,
                                                            dqv_computedOn=dataset_id, rdf_datatype='xsd:decimal',
                                                            ddqv_hasParameters=[],
                                                            dqv_value=evaluation_result,
                                                            dcterms_title=dataset_custom_metric.title)
                    measurements_list.append(measurement)
                    logger.debug(f'evaluation_result of ddqv_custom_metric={evaluation_result}')

        return measurements_list

    def custom_metrics_interpreter_kk(self, dataset_id: str, data_df: pd.DataFrame, kpis_df: pd.DataFrame):
        """
        Evaluate the list of rules included in the custom metrics stored in a Mongo DB and that are associated to the
        specified dataset. First of all, it gets the connection to the Mongo DB and then invokes to the corresponding
        method to do the actual evaluation.

        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement` in Json format
        """
        # Get DB connection
        mongodb_client = pymongo.MongoClient(self.rules_connection_string)
        mongodb_database = mongodb_client[self.rules_db_dbname]
        measurements_list = RuleInterpreter.compute(mongodb_database, dataset_id, data_df, kpis_df)
        # Convert from Pydantic model to JSON
        measurements_list_json = [ob.__dict__ for ob in measurements_list]
        # Close DB connection
        mongodb_client.close()
        return measurements_list_json

    def custom_metric_interpreter_kk(self, dataset_id: str, custom_metric_id: str, data_df: pd.DataFrame, kpis_df: pd.DataFrame):
        """
        Evaluate the list of rules included in the specified custom metric stored in a Mongo DB and that is associated
        to the the specified dataset. First of all, it gets the connection to the Mongo DB and then invokes to the
        corresponding method to do the actual evaluation.

        :param dataset_id: the dataset identifier.
        :type dataset_id: str
        :param custom_metric_id: the custom metric identifier.
        :type custom_metric_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement` in Json format
        """
        # Get DB connection
        mongodb_client = pymongo.MongoClient(self.rules_connection_string)
        mongodb_database = mongodb_client[self.rules_db_dbname]
        measurement = RuleInterpreter.compute_a_custom_metric(mongodb_database, dataset_id, custom_metric_id,
                                                                    data_df, kpis_df)
        measurement_json = measurement.model_dump_json()
        # Close DB connection
        mongodb_client.close()
        return measurement_json

    @staticmethod
    def compute_kk(db: pymongo.database.Database, dataset_id: str, data_df: pd.DataFrame, kpis_df: pd.DataFrame):
        """
        Evaluate the list of rules included in the custom metrics stored in a Mongo DB and that are associated to the
        specified dataset. First of all, it retrieves the custom metrics associated to the specified dataset and for
        each of the active custom metrics, evaluates the rules inside it.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement`
        """
        measurements_list = list()
        # Get custom metrics associated to dataset
        dataset_custom_metrics_db = rules_db_utils.get_active_custom_metrics_associated_to_dataset(db, dataset_id)
        if dataset_custom_metrics_db is not None:
            dataset_custom_metrics = models.DatasetCustomMetrics(**dataset_custom_metrics_db)
            now = datetime.now()
            date_time = now.strftime("%Y-%m-%dT%H:%M:%SZ")
            for custom_metric in dataset_custom_metrics.custom_metrics:
                if custom_metric.is_active:
                    # Only compute active rules
                    evaluation_result = custom_metrics_utils.custom_metric_interpreter(data_df, custom_metric, kpis_df)

                    measurement = models.QualityMeasurement(dqv_isMeasurementOf=custom_metric.id,
                                                            dqv_computedOn=dataset_id, rdf_datatype='xsd:decimal',
                                                            ddqv_hasParameters=[],
                                                            dqv_value=evaluation_result,
                                                            dcterms_title=custom_metric.title)
                    measurements_list.append(measurement)
                    logger.debug(f'evaluation_result of ddqv_custom_metric={evaluation_result}')
        return measurements_list

    @staticmethod
    def compute_a_custom_metric_kk(db: pymongo.database.Database, dataset_id: str, custom_metric_id: str,
                            data_df: pd.DataFrame, kpis_df: pd.DataFrame):
        """
        Evaluate the list of rules included in the specified custom metric stored in a Mongo DB and that is associated to the
        specified dataset. First of all, it retrieves the specified custom metric associated to the specified dataset
        and evaluates the rules inside it.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metric_id: the custom metric identifier.
        :type custom_metric_id: str
        :param data_df: the dataset.
        :type data_df: class:`pandas.DataFrame`
        :param kpis_df: pandas dataframe that contains the KPI values.
        :type kpis_df: class:`pandas.DataFrame`
        :return: the list of evaluation results of the evaluated custom metrics.
        :rtype: list of class:`models.QualityMeasurement`
        """
        measurement = {}
        # Get specified custom metric associated to dataset
        results = rules_db_utils.get_custom_metric_associated_to_dataset(db, dataset_id, custom_metric_id)
        dataset_custom_metric_db = None
        for ds in results:
            dataset_custom_metric_db = ds["ddqv_hasCustomMetric"]

        if dataset_custom_metric_db is not None:
            dataset_custom_metric = models.CustomMetric(**dataset_custom_metric_db)
            now = datetime.now()
            date_time = now.strftime("%Y-%m-%dT%H:%M:%SZ")
            evaluation_result = custom_metrics_utils.custom_metric_interpreter(data_df, dataset_custom_metric, kpis_df)

            measurement = models.QualityMeasurement(dqv_isMeasurementOf=dataset_custom_metric.id,
                                                    dqv_computedOn=dataset_id, rdf_datatype='xsd:decimal',
                                                    ddqv_hasParameters=[],
                                                    dqv_value=evaluation_result,
                                                    dcterms_title=dataset_custom_metric.title)
            logger.debug(f'evaluation_result of ddqv_custom_metric={evaluation_result}')
        else:
            raise custom_metrics_utils.CustomMetricNotFoundError(f"Custom metric '{custom_metric_id}' for dataset '{dataset_id}' not found.")

        return measurement

    def get_custom_metrics_req_kpis(self, dataset_id: str, custom_metrics_list: list = None):
        """
        Get the names of the KPI-s required by the custom metrics associated to a specific dataset, looked up by `dataset_id`.

        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of the names of the required KPI-s.
        :rtype: list of str
        """
        # Get DB connection
        mongodb_client = pymongo.MongoClient(self.rules_connection_string)
        mongodb_database = mongodb_client[self.rules_db_dbname]
        kpis = RuleInterpreter.get_custom_metrics_req_kpis_static(mongodb_database, dataset_id, custom_metrics_list)
        # Close DB connection
        mongodb_client.close()
        return kpis

    @staticmethod
    def get_custom_metrics_req_kpis_static(db: pymongo.database.Database, dataset_id: str, custom_metrics_list: list = None):
        """
        Get the names of the KPI-s required by the custom metrics associated to a specific dataset, looked up by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of the names of the required KPI-s.
        :rtype: list of str
        """
        if not custom_metrics_list:
            return RuleInterpreter.get_custom_metrics_req_kpis_dataset(db, dataset_id)
        else:
            return RuleInterpreter.get_custom_metrics_req_kpis_dataset_cm(db, dataset_id, custom_metrics_list)

    @staticmethod
    def get_custom_metrics_req_kpis_dataset(db: pymongo.database.Database, dataset_id: str):
        """
        Get the names of the KPI-s required by the custom metrics associated to a specific dataset, looked up by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :return: the list of the names of the required KPI-s.
        :rtype: list of str
        """
        kpis = []
        results = rules_db_utils.get_dataset_req_kpis(db, dataset_id)
        for ds in results:
            kpis = ds["kpi_col_list"]
        return kpis

    @staticmethod
    def get_custom_metrics_req_kpis_dataset_cm(db: pymongo.database.Database, dataset_id: str, custom_metrics_list: list):
        """
        Get the names of the KPI-s required by the custom metric associated to a specific dataset, looked up by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metric_id: the custom metric identifier.
        :type custom_metric_id: str
        :return: the list of the names of the required KPI-s.
        :rtype: list of str
        """
        kpis = []
        # Get required KPIs for the specified dataset and CustomMetric
        if custom_metrics_list is not None:
            for custom_metric_id in custom_metrics_list:
                results = rules_db_utils.get_custom_metric_req_kpis(db, dataset_id, custom_metric_id)
                for ds in results:
                    for kpi in ds["kpi_col_list"]:
                        if kpi not in kpis:
                            kpis.append(kpi)
        return kpis

    def get_custom_metrics_req_cols(self, dataset_id: str, custom_metrics_list: list = None):
        """
        Get the names of the dataset columns required by the custom metrics associated to a specific dataset, looked up
        by `dataset_id`.

        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of the names of the required dataset columns.
        :rtype: list of str
        """
        cols = []
        # Get DB connection
        mongodb_client = pymongo.MongoClient(self.rules_connection_string)
        mongodb_database = mongodb_client[self.rules_db_dbname]
        cols = RuleInterpreter.get_custom_metrics_req_cols_static(mongodb_database, dataset_id, custom_metrics_list)
        # Close DB connection
        mongodb_client.close()
        return cols

    @staticmethod
    def get_custom_metrics_req_cols_static(db: pymongo.database.Database, dataset_id: str, custom_metrics_list: list = None):
        """
        Get the names of the dataset columns required by the custom metrics associated to a specific dataset, looked up
        by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of the names of the required dataset columns.
        :rtype: list of str
        """
        if not custom_metrics_list or len(custom_metrics_list) == 0:
            return RuleInterpreter.get_custom_metrics_req_cols_dataset(db, dataset_id)
        else:
            return RuleInterpreter.get_custom_metrics_req_cols_dataset_cm(db, dataset_id, custom_metrics_list)

    @staticmethod
    def get_custom_metrics_req_cols_dataset(db: pymongo.database.Database, dataset_id: str):
        """
        Get the names of the dataset columns required by the custom metrics associated to a specific dataset, looked up
        by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :return: the list of the names of the required dataset columns.
        :rtype: list of str
        """
        cols = []
        # Get required columns for custom metrics involved in specified dataset
        results = rules_db_utils.get_dataset_req_cols(db, dataset_id)
        for ds in results:
            cols = ds["cols"]
        return cols

    @staticmethod
    def get_custom_metrics_req_cols_dataset_cm(db: pymongo.database.Database, dataset_id: str, custom_metrics_list: list):
        """
        Get the names of the dataset columns required by the custom metrics associated to a specific dataset, looked up
        by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :param custom_metrics_list: list of custom metric identifiers.
        :type custom_metrics_list: list
        :return: the list of the names of the required dataset columns.
        :rtype: list of str
        """
        cols = []
        # Get required columns for custom metrics involved in specified dataset
        if custom_metrics_list is not None:
            for custom_metric_id in custom_metrics_list:
                results = rules_db_utils.get_custom_metric_req_cols(db, dataset_id, custom_metric_id)
                for ds in results:
                    for col in ds["cols"]:
                        if col not in cols:
                            cols.append(col)
        return cols

    def check_dataset_active_custom_metrics(self, dataset_id: str):
        """
        Check if specified dataset has active custom metrics associated to it, looked up by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :return: True/False, if dataset has or no active custom metrics associated to it.
        :rtype: boolean
        """
        # Get active custom metrics from Rules repo
        # Get DB connection
        mongodb_client = pymongo.MongoClient(self.rules_connection_string)
        mongodb_database = mongodb_client[self.rules_db_dbname]
        check_active_custom_metrics = RuleInterpreter.check_dataset_active_custom_metrics_static(mongodb_database, dataset_id)
        # Close DB connection
        mongodb_client.close()
        return check_active_custom_metrics

    @staticmethod
    def check_dataset_active_custom_metrics_static(db: pymongo.database.Database, dataset_id: str):
        """
        Check if specified dataset has active custom metrics associated to it, looked up by `dataset_id`.

        :param db: the database connection.
        :type db: class:`pymongo.database.Database`
        :param dataset_id: the dataset identifier, required to retrieve the associated custom metrics from the Mongo DB.
        :type dataset_id: str
        :return: True/False, if dataset has or no active custom metrics associated to it.
        :rtype: boolean
        """
        # Get active custom metrics from Rules repo
        dataset_custom_metrics_db = rules_db_utils.get_active_custom_metrics_associated_to_dataset(db, dataset_id)
        if dataset_custom_metrics_db:
            return True
        else:
            return False

