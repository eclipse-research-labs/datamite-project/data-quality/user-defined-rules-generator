"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
"""

"""
This module contains the functions used to evaluate the rules inside the custom metrics.
"""
import logging
import pandas as pd

from ruleinterpreter import models
from typing import List


logger = logging.getLogger(__name__)


class CustomMetricNotFoundError(Exception):
	"""
	This class is the type of exception thrown when a custom metric is not found.
	"""
	def __init__(self, message):
		super().__init__(message)


class ColumnNotFoundError(Exception):
	"""
	This class is the type of exception thrown when a required column is not provided.
	"""
	def __init__(self, message):
		super().__init__(message)


class KPINotFoundError(Exception):
	"""
	This class is the type of exception thrown when a required KPI is not provided.
	"""
	def __init__(self, message):
		super().__init__(message)


def get_kpi(kpis_df: pd.DataFrame, column_name: str, kpi_name: str):
	"""
	Obtains the value of a kpi for the column specified as input parameter.

	:param kpis_df: pandas dataframe that contains the KPI values.
	:type kpis_df: class:`pandas.DataFrame`
	:param column_name: the column name for which the KPI must be obtained.
	:type column_name: str
	:param kpi_name: the KPI name.
	:type kpi_name: str
	:return: the value of the KPI.
	:rtype: str
	"""
	try:
		kpi_row = kpis_df.loc[(kpis_df['col'] == column_name) & (kpis_df['kpi'] == kpi_name)]
		return kpi_row['value'].iloc[0]
	except:
		raise KPINotFoundError(f"KPI '{kpi_name}' for column '{column_name}' not found.")


def get_rdf_value(rdf_val: str):
	"""
	Convert the value specified in RDF format into Python type.

	:param rdf_val: the value in RDF format. E.g.: "-50.0"^^xsd:double, "Bilbao"^^xsd:string.
	:type rdf_val: str
	:return: the value in Python.
	:rtype: str, float
	"""
	#  Examples:
	#  "-50.0"^^xsd:double --> "\"-50.0\"^^xsd:double"
	#  "Bilbao"^^xsd:string --> "\"Bilbao\"^^xsd:string"
	#  "Bilbao"
	val = 0.0
	rdf_val_lst = rdf_val.split('^^')
	# TODO: Type could be rdf:string, rdf:dateTime, rdf:double, rdf:decimal, xsd:integer, xsd:float, etc.
	if len(rdf_val_lst) > 1:
		if rdf_val_lst[1] in ['xsd:double', 'xsd:decimal', 'xsd:integer', 'xsd:float']:
			if '\"' in rdf_val_lst[0]:
				val = float(rdf_val_lst[0].split('\"')[1])
			else:
				val = float(rdf_val_lst[0].split("\'")[1])
		elif rdf_val_lst[1] == 'xsd:string':
			if '\"' in rdf_val_lst[0]:
				val = rdf_val_lst[0].split('\"')[1]
			else:
				val = rdf_val_lst[0].split("\'")[1]
			val = f'\"{val}\"'
	else:
		val = f'\"{rdf_val}\"'
	return val


def operator_translation(operator):
	"""
	Convert the rule operator into string format.

	:param operator: the rule operator.
	:type operator: class:`models.Operator`
	:return: the rule operator in string format.
	:rtype: str
	"""
	python_operator = '=='

	if operator == models.Operator.EQ:
		python_operator = '=='
	elif operator == models.Operator.NEQ:
		python_operator = '!='
	elif operator == models.Operator.LT:
		python_operator = '<'
	elif operator == models.Operator.LTEQ:
		python_operator = '<='
	elif operator == models.Operator.GT:
		python_operator = '>'
	elif operator == models.Operator.GTEQ:
		python_operator = '>='
	elif operator == models.Operator.IN:
		python_operator = '.isin'
	elif operator == models.Operator.REGEXMATCH:
		python_operator = '.str.contains'

	return python_operator


def apply_modif_to_operand(parsed_operand: str, op_modifier: str):
	"""
	Apply the corresponding modification  to the parsed operand.

	:param parsed_operand: the parsed operand.
	:type parsed_operand: str
	:param op_modifier: the modification to apply to the parsed operand.
	:type op_modifier: str
	:return: the modified operand.
	:rtype: str
	"""
	modif_operand = parsed_operand
	if op_modifier is not None and len(op_modifier.strip()) > 0:
		modif_operand = op_modifier.replace('x', str(parsed_operand))
	return modif_operand


def parse_rule_operand_col_val(referring_operand: models.RuleOperandRefersTo, column_names_list: list, data_df: pd.DataFrame):
	"""
	Convert the column value related operand into a Python expression, so that it can be evaluated.

	:param referring_operand: the operand.
	:type referring_operand: class:`models.RuleOperandRefersTo`
	:param column_names_list: list of the names of the columns provided.
	:type column_names_list: list of str
	:param data_df: the dataset.
	:type data_df: class:`pandas.DataFrame`
	:return: the parsed operand.
	:rtype: str
	"""
	# Check if column is provided
	if referring_operand.column_name not in column_names_list:
		raise ColumnNotFoundError(f"Column '{referring_operand.column_name}' not provided.")
	parsed_operand = 'data_df["' + referring_operand.column_name + '"]'
	parsed_operand = apply_modif_to_operand(parsed_operand, referring_operand.op_modifier)
	# Check if datetime type
	if referring_operand.date_format is not None and len(referring_operand.date_format.strip()) > 0:
		# If datetime type, convert column values into datetime format
		if not pd.api.types.is_datetime64_any_dtype(data_df[referring_operand.column_name]):
			data_df[referring_operand.column_name] = pd.to_datetime(data_df[referring_operand.column_name],
																format=referring_operand.date_format)
	return parsed_operand


def parse_rule_operand_kpi(referring_operand: models.RuleOperandRefersTo, kpis_df: pd.DataFrame):
	"""
	Convert the KPI related operand into a Python expression.

	:param referring_operand: the operand.
	:type referring_operand: class:`models.RuleOperandRefersTo`
	:param kpis_df: pandas dataframe that contains the KPI values.
	:type kpis_df: class:`pandas.DataFrame`
	:return: the parsed operand.
	:rtype: str
	"""
	# Get KPI name and column name
	kpi_name = referring_operand.refers_to
	column_name = referring_operand.column_name
	# Obtain KPI of dataset X. KPI-s names should match names used inside rule
	parsed_operand = get_kpi(kpis_df, column_name, kpi_name)
	parsed_operand = apply_modif_to_operand(parsed_operand, referring_operand.op_modifier)
	return parsed_operand


def parse_rule_operand_val(value_operand: models.RuleOperandValue):
	"""
	Convert the Value related operand into a Python expression.

	:param value_operand: the operand.
	:type value_operand: class:`models.RuleOperandValue`
	:return: the parsed operand.
	:rtype: str
	"""
	# Convert right operand to number/str
	parsed_operand = get_rdf_value(value_operand.operand_value)
	# Check if datetime type
	if isinstance(parsed_operand, str) and value_operand.date_format is not None and \
			len(value_operand.date_format.strip()) > 0:
		# If datetime type, convert value into datetime format
		parsed_operand = 'pd.to_datetime(' + parsed_operand + ', format="' + value_operand.date_format + '")'
	return parsed_operand


def parse_rule_operand(operand: models.RuleOperand, kpis_df: pd.DataFrame, column_names_list: list, data_df: pd.DataFrame):
	"""
	Convert operand into a Python expression, so that it can be evaluated. The operand could be: the value of a column,
	a kpi value or a value.

	:param operand: the operand.
	:type operand: class:`models.RuleOperand`
	:param kpis_df: pandas dataframe that contains the KPI values.
	:type kpis_df: class:`pandas.DataFrame`
	:param column_names_list: list of the names of the columns provided.
	:type column_names_list: list of str
	:param data_df: the dataset.
	:type data_df: class:`pandas.DataFrame`
	:return: the Python expression.
	:rtype: str
	"""
	parsed_operand = ''

	if operand.referring_operand:
		if 'columnvalue' in operand.referring_operand.refers_to.lower():
			# Column value
			parsed_operand = parse_rule_operand_col_val(operand.referring_operand, column_names_list, data_df)
		else:
			# KPI
			parsed_operand = parse_rule_operand_kpi(operand.referring_operand, kpis_df)
	elif operand.value_operand:
		parsed_operand = parse_rule_operand_val(operand.value_operand)
	return parsed_operand


def operator_evaluator_column_value(data_df: pd.DataFrame, column_val_related_rules: List[models.Rule],
									logical_operator: str, kpis_df: pd.DataFrame):
	"""
	Convert list of rules into a Python expression, so that they can be evaluated, and evaluate them. The operands in
	the rules will refer to columns in the dataset.

	:param data_df: the dataset.
	:type data_df: class:`pandas.DataFrame`
	:param column_val_related_rules: list of rules to be evaluated.
	:type column_val_related_rules: class:`List[models.Rule]`
	:param logical_operator: the logical operator used to concatenate the rules (AND or OR).
	:type logical_operator: str
	:param kpis_df: pandas dataframe that contains the KPI values.
	:type kpis_df: class:`pandas.DataFrame`
	:return: the evaluation result.
	:rtype: float
	"""
	eval_result_percent = 0.0
	filtered_data_df = pd.DataFrame()

	if len(column_val_related_rules) > 0:
		# Generate logical expression for Pandas dataframe column filtering
		# e.g: data_df.loc[(data_df[column_name] <= right_operand) | & (data_df[column_name] >= right_operand)]
		# e.g.: data_df.loc[(data_df.foo > data_df.bar) | & (data_df.bar > data_df.baz)]
		num_rules_to_evaluate = 0
		eval_expression = 'data_df.loc['
		for index, rule in enumerate(column_val_related_rules):
			try:
				left_operand = parse_rule_operand(models.RuleOperand(referring_operand=rule.left_operand), kpis_df,
												  list(data_df.columns), data_df)
				right_operand = parse_rule_operand(rule.right_operand, kpis_df, list(data_df.columns), data_df)
				if rule.operator == models.Operator.IN:
					right_operand = right_operand.strip('\"')
					right_operand = right_operand.replace("\'", '"')
				eval_expression = eval_expression + '(' + str(left_operand)\
														+ operator_translation(rule.operator) + '(' + str(right_operand) + '))'
				if index < (len(column_val_related_rules) - 1):
					# Add logical operator to expression
					if logical_operator == models.LogicalOperator.AND:
						eval_expression = eval_expression + ' & '
					else:
						eval_expression = eval_expression + ' | '
				num_rules_to_evaluate = num_rules_to_evaluate + 1
			except ColumnNotFoundError as col_error:
				logger.error(f"{col_error}")
				if logical_operator == models.LogicalOperator.AND:
					num_rules_to_evaluate = 0
					break
			except KPINotFoundError as kpi_error:
				logger.error(f"{kpi_error}")
				if logical_operator == models.LogicalOperator.AND:
					num_rules_to_evaluate = 0
					break

		if num_rules_to_evaluate > 0:
			eval_expression = eval_expression + ']'
			filtered_data_df = eval(eval_expression)
			eval_result_percent = len(filtered_data_df)*100/len(data_df)

	return eval_result_percent


def operator_evaluator_kpi(left_operand, operator, right_operand):
	"""
	Compose a Python expression using the provided input operands and operator, so that it can be evaluated, and
	evaluate it. The operands will be simple Python values.

	:param left_operand: the left operand.
	:type left_operand:str
	:param operator: the operator.
	:type operator: str
	:param right_operand: the right operand.
	:type right_operand:str
	:return: the evaluation result.
	:rtype: float
	"""
	eval_result_percent = 0.0

	if operator == models.Operator.IN:
		right_operand = right_operand.strip('\"')
	eval_expression = str(left_operand) + operator_translation(operator) + '(' + str(right_operand) + ')'
	eval_result = eval(eval_expression)
	# Convert boolean evaluation result (true/false) into numeric value
	if eval_result:
		eval_result_percent = 100.0

	return eval_result_percent


def custom_metric_interpreter(data_df: pd.DataFrame, custom_metric: models.CustomMetric, kpis_df: pd.DataFrame):
	"""
	Evaluate the list of rules included in the provided custom metric.

	:param data_df: the dataset.
	:type data_df: class:`pandas.DataFrame`
	:param custom_metric: the custom metric containing the list of rules to be evaluated.
	:type custom_metric: class:`models.CustomMetric`
	:param kpis_df: pandas dataframe that contains the KPI values.
	:type kpis_df: class:`pandas.DataFrame`
	:return: the evaluation result.
	:rtype: float
	"""
	total_evaluation_result = 0.0

	evaluation_result_lst = list()
	column_val_related_rules = list()
	column_kpi_related_rules = list()
	for rule in custom_metric.has_rule:
		if ('columnvalue' in rule.left_operand.refers_to.lower()) or\
				((rule.right_operand.referring_operand) and ('columnvalue' in rule.right_operand.referring_operand.refers_to.lower())):
			column_val_related_rules.append(rule)
		else:
			column_kpi_related_rules.append(rule)

	for rule in column_kpi_related_rules:
		try:
			right_operand = parse_rule_operand(rule.right_operand, kpis_df, list(data_df.columns), data_df)
			# Left operand is a KPI
			kpi = parse_rule_operand_kpi(rule.left_operand, kpis_df)
			eval_result = operator_evaluator_kpi(kpi, rule.operator, right_operand)
			evaluation_result_lst.append(eval_result)
		except ColumnNotFoundError as col_error:
			logger.error(f"{col_error}")
		except KPINotFoundError as kpi_error:
			logger.error(f"{kpi_error}")

	if len(column_val_related_rules) > 0:
		eval_result = operator_evaluator_column_value(data_df, column_val_related_rules,
												custom_metric.logical_operator, kpis_df)
		evaluation_result_lst.append(eval_result)

	# Check logical operator
	if len(evaluation_result_lst) > 0:
		if custom_metric.logical_operator == models.LogicalOperator.AND:
			total_evaluation_result = min(evaluation_result_lst)
		else:
			total_evaluation_result = max(evaluation_result_lst)
	return total_evaluation_result
