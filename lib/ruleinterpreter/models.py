"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
"""

"""
This module contains the classes used to structure the rules, CustomMetric-s  and QualityMeasurement-s.
"""
import uuid
from datetime import datetime, timedelta
from enum import Enum
from pydantic import BaseModel, Field, ConfigDict, field_validator, model_validator, ValidationInfo
from typing import List, Optional, Literal
from ruleinterpreter.mongo_utils import PyObjectId


class LogicalOperator(str, Enum):
    """
    Enumerate the possible logical operators supported to concatenate the rules inside a custom metric.
    """
    AND = 'ddqv:and'
    OR = 'ddqv:or'


class Operator(str, Enum):
    """
    Enumerate the possible operators supported to compare the operands in a rule of a custom metric.
    """
    EQ = 'ddqv:eq'
    NEQ = 'ddqv:neq'
    LT = 'ddqv:lt'
    GT = 'ddqv:gt'
    LTEQ = 'ddqv:lteq'
    GTEQ = 'ddqv:gteq'
    IN = 'ddqv:in'
    REGEXMATCH = 'ddqv:regexMatch'


class RuleOperandRefersTo(BaseModel):
    """
    This class represents the operand of type refers_to inside a rule of a custom metric.
    """
    column_name: str = Field(alias='ddqv_columnName')
    refers_to: str = Field(alias='ddqv_refersTo')
    date_format: Optional[str] = Field(alias='ddqv_dateFormat', default=None)
    op_modifier:  Optional[str] = Field(alias='ddqv_opModificator', default=None)

    @field_validator('date_format')
    def correct_date_format(cls, v):
        if v is not None and len(v.strip()) > 0:
            now = datetime.now()
            try:
                now.strftime(v)
            except:
                raise ValueError('not valid date format')
        return v

    @field_validator('op_modifier')
    def correct_op_modifier(cls, v):
        if v is not None and len(v.strip()) > 0:
            if not 'timedelta' in v:
                modif_v = v.replace('x', '1')
                try:
                    eval(modif_v)
                except:
                    raise ValueError('not valid arithmetic expression')
            else:
                now = datetime.now()
                modif_v = v.replace('x', 'now')
                try:
                    eval(modif_v)
                except:
                    raise ValueError('not valid expression for dates')
        return v


class RuleOperandValue(BaseModel):
    """
    This class represents the operand of type value inside a rule of a custom metric.
    """
    operand_value: str = Field(alias='ddqv_operandValue')
    date_format: Optional[str] = Field(alias='ddqv_dateFormat', default=None)

    @model_validator(mode='after')
    def check_date_format(self):
        operand_value = self.operand_value
        date_format = self.date_format
        if date_format and len(date_format.strip()) > 0:
            val = ''
            rdf_val_lst = operand_value.split('^^')
            if len(rdf_val_lst) > 1:
                if rdf_val_lst[1] == 'xsd:string':
                    val = rdf_val_lst[0].split('\"')[1]
                else:
                    raise ValueError('operand_value must be a string type')
            else:
                val = operand_value
            try:
                datetime.strptime(val, date_format)
            except:
                raise ValueError('not valid date format')

        return self


class RuleOperand(BaseModel):
    """
    This class represents an operand inside a rule of a custom metric.
    """
    referring_operand: Optional[RuleOperandRefersTo] = None
    value_operand: Optional[RuleOperandValue] = None


class Rule(BaseModel):
    """
    This class represents a rule of a custom metric.
    """
    rdf_type: Optional[str] = Field(alias='rdf_type', default='ddqv:Rule')
    left_operand: RuleOperandRefersTo = Field(alias='ddqv_leftOperand')
    operator: Operator = Field(alias='ddqv_operator')
    right_operand: RuleOperand = Field(alias='ddqv_rightOperand')


class NewCustomMetric(BaseModel):
    """
    This class represents a new custom metric.
    """
    rdf_type: Optional[str] = Field(alias='rdf_type', default='ddqv:CustomMetric')
    title: str = Field(alias='dcterms_title')
    description: str = Field(alias='skos_definition')
    expected_data_type: Optional[str] = Field(alias='dqv_expectedDataType', default='xsd:double')
    dimension: str = Field(alias='dqv_inDimension')
    logical_operator: LogicalOperator = Field(alias='ddqv_logicalOperator')
    generated_by: str = Field(alias='prov_wasGeneratedBy')
    has_rule: List[Rule] = Field(alias='ddqv_hasRule')
    is_active: bool = Field(alias='is_active')
    weight: Optional[float] = None
    lower_threshold: Optional[float] = None
    upper_threshold: Optional[float] = None


class CustomMetric(NewCustomMetric):
    """
    This class represents a custom metric.
    """
    id: str = Field(alias='dcterms_identifier')

    @classmethod
    def from_new_custom_metric(cls, new_cm: NewCustomMetric) -> dict:
        """
        Create a CustomMetric object from an existing NewCustomMetric object, by adding a randomly generated id.
        """
        cm_dict =new_cm.dict(by_alias=True)
        # Generate a random id
        cm_dict['dcterms_identifier'] = str(uuid.uuid4())
        cls.model_validate(cm_dict)
        return cls.model_validate(cm_dict)


class DatasetCustomMetrics(BaseModel):
    """
    This class represents the list of custom metrics associated to a dataset.
    """
    id: Optional[PyObjectId] = Field(alias="_id", default=None)
    dataset_id: str = Field(alias='dataset_id')
    custom_metrics: List[CustomMetric] = Field(alias='ddqv_hasCustomMetric')

    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )


class QualityMeasurement(BaseModel):
    """
    This class represents the value obtained when evaluating a custom metric.
    """
    dqv_isMeasurementOf: str
    dqv_computedOn: str
    rdf_datatype: str
    ddqv_hasParameters: list
    dqv_value: float
    dcterms_title: str


class DatasetCustomMetricActiveStatus(BaseModel):
    """
    This class is used to specify if the custom metric is active or not.
    """
    id: str
    is_active: bool
