"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

SPDX-License-Identifier: MIT
"""

"""
This module contains the functions used to evaluate the rules inside the custom metrics.
"""
import logging
import pymongo

logger = logging.getLogger(__name__)


def get_active_custom_metrics_associated_to_dataset(db: pymongo.database.Database, dataset_id: str):
    # Get custom metrics associated to dataset
    dataset_custom_metrics_db = db.get_collection("qualityRules").find_one({"dataset_id": dataset_id,
                                                                            "ddqv_hasCustomMetric.is_active": True})
    return dataset_custom_metrics_db


def get_custom_metric_associated_to_dataset(db: pymongo.database.Database, dataset_id: str, custom_metric_id: str):
    # Get specified custom metric associated to dataset
    pipeline = [
        {"$match": {
            "$and": [{"dataset_id": dataset_id}, {"ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}]}},
        {"$unwind": "$ddqv_hasCustomMetric"},
        {"$match": {"ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
        {"$project": {"_id": 0, "ddqv_hasCustomMetric": "$ddqv_hasCustomMetric"}}
    ]

    results = db.get_collection("qualityRules").aggregate(pipeline)
    return results


def get_dataset_req_kpis(db: pymongo.database.Database, dataset_id: str):
    # Get required KPIs for custom metrics involved in specified dataset
    pipeline = [
        {"$match": {"dataset_id": dataset_id}},
        {"$unwind": "$ddqv_hasCustomMetric"},
        {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
        {"$match": {"ddqv_hasCustomMetric.is_active": True,
                    "ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_refersTo": {"$exists": True,
                                                                                         "$ne": "ddqv:columnValue"}}},
        {"$project": {"_id": 0, "dataset_id": 1,
                      "kpi_col": {"col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_columnName",
                                  "kpi": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_refersTo"}}},

        {"$unionWith": {"coll": "qualityRules", "pipeline": [
            {"$match": {"dataset_id": dataset_id}},
            {"$unwind": "$ddqv_hasCustomMetric"},
            {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
            {"$match": {"ddqv_hasCustomMetric.is_active": True,
                        "ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_refersTo":
                            {"$exists": True, "$ne": "ddqv:columnValue"}}},
            {"$project": {	"_id": 0, "dataset_id": 1, "kpi_col":
                {"col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_columnName",
                 "kpi": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_refersTo"}}},
        ]}},

        {"$group": {"_id": "$dataset_id", "kpi_col_list": {"$addToSet": "$kpi_col"}}},
    ]

    results = db.get_collection("qualityRules").aggregate(pipeline)
    return results


def get_custom_metric_req_kpis(db: pymongo.database.Database, dataset_id: str, custom_metric_id: str):
    # Get required KPIs for the specified dataset and CustomMetric
    pipeline = [
        {"$match": {"dataset_id": dataset_id, "ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
        {"$unwind": "$ddqv_hasCustomMetric"},
        {"$match": {"ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
        {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
        {"$match": {"ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_refersTo": {"$exists": True,
                                                                                         "$ne": "ddqv:columnValue"}}},
        {"$project": {"_id": 0, "dataset_id": 1,
                      "kpi_col": {"col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_columnName",
                                  "kpi": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_refersTo"}}},

        {"$unionWith": {"coll": "qualityRules", "pipeline": [
            {"$match": {"dataset_id": dataset_id, "ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
            {"$unwind": "$ddqv_hasCustomMetric"},
            {"$match": {"ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
            {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
            {"$match": {"ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_refersTo":
                            {"$exists": True, "$ne": "ddqv:columnValue"}}},
            {"$project": {	"_id": 0, "dataset_id": 1, "kpi_col":
                {"col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_columnName",
                 "kpi": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_refersTo"}}},
        ]}},

        {"$group": {"_id": "$dataset_id", "kpi_col_list": {"$addToSet": "$kpi_col"}}},
    ]

    results = db.get_collection("qualityRules").aggregate(pipeline)
    return results


def get_dataset_req_cols(db: pymongo.database.Database, dataset_id: str):
    # Get required columns for custom metrics involved in specified dataset
    pipeline = [
        {"$match": {"dataset_id": dataset_id}},
        {"$unwind": "$ddqv_hasCustomMetric"},
        {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
        {"$match": {"ddqv_hasCustomMetric.is_active": True,
                    "ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_refersTo": {"$exists": True,
                                                                                         "$eq": "ddqv:columnValue"}}},
        {"$project": {"_id": 0, "dataset_id": 1,
                      "col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_columnName"}},

        {"$unionWith": {"coll": "qualityRules", "pipeline": [
            {"$match": {"dataset_id": dataset_id}},
            {"$unwind": "$ddqv_hasCustomMetric"},
            {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
            {"$match": {"ddqv_hasCustomMetric.is_active": True,
                        "ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_refersTo":
                            {"$exists": True, "$eq": "ddqv:columnValue"}}},
            {"$project": {	"_id": 0, "dataset_id": 1, "col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_columnName"}},
        ] } },

        { "$group": {"_id": "$dataset_id", "cols": {"$addToSet": "$col"}}},
    ]

    results = db.get_collection("qualityRules").aggregate(pipeline)
    return results


def get_custom_metric_req_cols(db: pymongo.database.Database, dataset_id: str, custom_metric_id: str):
    # Get required columns for custom metrics involved in specified dataset
    pipeline = [
        {"$match": {"dataset_id": dataset_id, "ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
        {"$unwind": "$ddqv_hasCustomMetric"},
        {"$match": {"ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
        {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule"},
        {"$match": {"ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_refersTo": {"$exists": True,
                                                                                         "$eq": "ddqv:columnValue"}}},
        {"$project": {	"_id": 0, "dataset_id": 1, "col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_leftOperand.ddqv_columnName"}},

        { "$unionWith": { "coll": "qualityRules", "pipeline": [
            {"$match": {"dataset_id" :dataset_id, "ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
            {"$unwind": "$ddqv_hasCustomMetric" },
            {"$match": {"ddqv_hasCustomMetric.dcterms_identifier": custom_metric_id}},
            {"$unwind": "$ddqv_hasCustomMetric.ddqv_hasRule" },
            {"$match": {"ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_refersTo" :
                            {"$exists": True, "$eq" :"ddqv:columnValue"}}},
            {"$project" : {	 "_id": 0, "dataset_id": 1, "col": "$ddqv_hasCustomMetric.ddqv_hasRule.ddqv_rightOperand.referring_operand.ddqv_columnName"}},
        ] } },

        { "$group": {"_id": "$dataset_id", "cols": {"$addToSet": "$col"}}},
    ]

    results = db.get_collection("qualityRules").aggregate(pipeline)
    return results
